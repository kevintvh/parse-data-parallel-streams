# parse-data-parallel-streams



#
# How to run ?
#

1. Unzip data files from src/resoures/test-files.zip
2. Run DataParser with number of parallel threads parameter

#
# How it work 
#

For simplicity, this only parses data files with record as a line, not put into a bean and serialization.

Pseudo code:

```
Input:
    parallelParam

Compute:
    dataInMemory = parallelRead(data_files)
    dataSize = calculateDataSize(dataInMemory)
    chunkDataSize = calculateChunkDataSize(dataSize, parallelParam)
    
Output:
    parallelWrite(data, chunkDataSize, parallelParam)

```

