import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataParser {
    private static Stream<String> dataStream = Stream.<String>builder().build();

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new RuntimeException("Please set the number of parallel streams parameter !");
        }

        int parallelism = Integer.valueOf(args[0]);

        Iterator<Path> paths = null;
        try {
            paths = Files.newDirectoryStream(Paths.get("src/main/resources/test-files"),
                    path -> path.toString().endsWith(".dat"))
                    .iterator();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (paths == null) {
            return;
        }

        ForkJoinPool forkJoinPool = new ForkJoinPool(parallelism);
        try {
            Iterator<Path> finalPaths = paths;
            forkJoinPool.submit(() -> {
                        finalPaths.forEachRemaining(path -> {
                            dataStream = Stream.concat(dataStream, processInputFile(path.toString()));
                        });
                    }
            ).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Collection<String> data = dataStream.collect(Collectors.toList());

        int chunkSize = data.size() / parallelism;
        final AtomicInteger counter = new AtomicInteger();

        Collection<List<String>> result = data.parallelStream().collect(Collectors.groupingBy(it -> counter.getAndIncrement() / chunkSize)).values();

        forkJoinPool = new ForkJoinPool(parallelism);
        try {
            forkJoinPool.submit(() -> {
                        result.parallelStream().forEach(list -> {
                                    FileWriter writer = null;
                                    try {
                                        writer = new FileWriter(UUID.randomUUID() + ".result");

                                        for (String str : list) {
                                            writer.write(str + "\n");
                                        }

                                        writer.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                        );

                    }
            ).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static Stream<String> processInputFile(String inputFilePath) {
        List<String> inputList = new ArrayList<>();

        BufferedReader br = null;
        try {
            File inputF = new File(inputFilePath);
            InputStream inputFS = new FileInputStream(inputF);
            br = new BufferedReader(new InputStreamReader(inputFS));

            inputList = br.lines().collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return inputList.stream();
    }

}